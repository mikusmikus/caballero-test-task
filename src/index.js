import "./style/index.scss";

const navbar = document.querySelector(".js-header__navbar");
const hamburger = document.querySelector(".js-hamburger");
const navWrapper = document.querySelector(".js-header__navbar-wrapper");
const languages = document.querySelectorAll(".js-header__language");
const navbarLinks = document.querySelectorAll(".js-nav-link");

let toggle = 1;
hamburger.addEventListener("click", () => {
  if (toggle > 0) {
    hamburger.classList.add("active");
    navWrapper.classList.add("active");
    navbar.classList.add("active");
  } else {
    navWrapper.classList.remove("active");
    navbar.classList.remove("active");
    hamburger.classList.remove("active");
  }
  toggle = toggle * -1;
});

languages.forEach((language) => {
  language.addEventListener("click", () => {
    navWrapper.classList.remove("active");
    navbar.classList.remove("active");
    hamburger.classList.remove("active");
    toggle = toggle * -1;
  });
});

let linkToggle = 1;
navbarLinks.forEach((link) => {
  link.addEventListener("click", () => {
    if (linkToggle > 0) {
      link.classList.add("active");
    } else {
      link.classList.remove("active");
    }
    linkToggle = linkToggle * -1;
  });
});

window.onscroll = () => scrollFunction();

const scrollFunction = () => {
  if (
    document.body.scrollTop > 800 ||
    document.documentElement.scrollTop > 800
  ) {
    navWrapper.classList.remove("active");
    navbar.classList.remove("active");
    hamburger.classList.remove("active");
    navbarLinks.forEach((link) => {
      link.classList.remove("active");
    });
    linkToggle = 1;
  }
};
